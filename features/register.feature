Feature: Visitor registers on Redmine website

  Scenario: Visitor can register on Redmine website
    Given I visit registration page to create an account
    When I put my registration data in the register form
    Then  Account is successfully created
