Given(/^I visit registration page to create an account$/) do
  visit 'https://testautomate.me/redmine/account/register'
end

When(/^I put my registration data in the register form$/) do
  fill_in('Login', with:'TestnQA1')
  fill_in('Password ', with:'Qwerty31286')
  fill_in('Confirmation', with:'Qwerty31286')
  fill_in('First name', with:'Valeriy1')
  fill_in('Last name', with:'Drozd1')
  fill_in('Email', with:'qaqapomidoru@gmail.com')
  uncheck('pref_hide_mail')
  click_button('Submit')
end

Then(/^Account is successfully created$/) do
  expect(page).to have_content 'Your account has been activated. You can now log in.'

  sleep 5
end